//
//  ViewController.swift
//  ARWebView
//
//  Created by Dean Murphy on 23/05/2019.
//  Copyright © 2019 eyeo GmbH. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate, UITextFieldDelegate {

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var urlTextField: UITextField!
    @IBAction func newTabAction(_ sender: Any) {
        let textfieldURL = "https://\(urlTextField.text!)"
        createNewTab(withURL: textfieldURL)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the view's delegate
        sceneView.delegate = self

        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        sceneView.debugOptions = [ARSCNDebugOptions.showWorldOrigin,
                                  ARSCNDebugOptions.showFeaturePoints]
        sceneView.autoenablesDefaultLighting = true
        print(sceneView.isUserInteractionEnabled)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    @objc func createNewTab(withURL: String){
        guard let currentFrame = self.sceneView.session.currentFrame else {
            return
        }
        // create a web view
        let webView = UIWebView(frame: CGRect(x: 0, y: 0, width: 640, height: 480))
        let request = URLRequest(url: URL(string: withURL)!)

        webView.loadRequest(request)

        let webviewPlane = SCNPlane(width: 1.0, height: 0.75)
        webviewPlane.firstMaterial?.diffuse.contents = webView
        webviewPlane.firstMaterial?.isDoubleSided = true

        let webviewPlaneNode = SCNNode(geometry: webviewPlane)

        var translation = matrix_identity_float4x4
        translation.columns.3.z = -1.0

        webviewPlaneNode.simdTransform = matrix_multiply(currentFrame.camera.transform, translation)
        webviewPlaneNode.eulerAngles = SCNVector3(0,0,0)

        self.sceneView.scene.rootNode.addChildNode(webviewPlaneNode)

    }
}
